Huddle landing page with alternating feature blocks

My challenge was to build out this landing page from the designs provided in the starter code.

Read more about this challenge 
https://www.frontendmentor.io/challenges/huddle-landing-page-945317

More challanges from Frontend Mentor 
https://www.frontendmentor.io/?ref=challenge
